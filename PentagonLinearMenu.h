#pragma once
#include "ffShapeDrawer.h"
#include <cmath>
#include <string>
class PentagonLinearMenu
{
private:
	unsigned int currentMenuIndex;
	unsigned int numberMenus;
	string * characters;
	string * descriptions;
	float fade;
	bool isEnter;
	ofTrueTypeFont mySmallFont;
	ofTrueTypeFont myBigFont;
public:
	PentagonLinearMenu(void)
	{
		currentMenuIndex = 0;
		numberMenus = 5;
		descriptions = new string[numberMenus];
		descriptions[0] = "Easy";
		descriptions[1] = "Medium";
		descriptions[2] = "Hard";
		descriptions[3] = "Impossible";
		descriptions[4] = "Exit";
		fade = 0.001;
		isEnter = false; 
		mySmallFont.loadFont("verdana.ttf", 16);
		myBigFont.loadFont("verdana.ttf", 24);
	}
	~PentagonLinearMenu(void)
	{
		delete [] descriptions;
	}
	void cycleLeft(){
		(--currentMenuIndex += numberMenus) %= numberMenus;
	}
	void cycleRight()
	{
		(++currentMenuIndex) %= numberMenus;
	}
	unsigned int enterMenu()
	{
		isEnter = true;
		return currentMenuIndex;
	}
	//returns integer representing which menu was selected
	int updateMenu()
	{
		if(isEnter)
			fade += 0.03;
		if(fade>=1)
			return currentMenuIndex;
		return -1;
	}
	void drawMenu()
	{
		if(fade >= 1)
			return;
		vector<bool> walls(5, true);
		int radius = 50;
		//calculate center of screen
		int centerX = ofGetWidth()/2.0;
		int centerY = (int)(ofGetHeight()*0.35f);
		float horizontalOffset = sin(TWO_PI/5.0)*radius;
		float leftStart = centerX - (numberMenus/2) * horizontalOffset;
		float heightOffset = radius + cos(TWO_PI/5.0)*radius;
		
		
		for(int i = 0; i < numberMenus; i++)
		{
			walls[0] = false;
			walls[4] = false;
			if(i == 0)
				walls[4] = true;
			if(i == numberMenus -1)
			{
				if((numberMenus-1)%2)
					walls[4] = true;
				else
					walls[0] = true;
			}
			float x = leftStart + i*horizontalOffset;
			float y = centerY + heightOffset*(i%2);
			if(i == currentMenuIndex)
			{
				drawPentagon(i%2,x,y,walls,(1-fade));
			}
			else
				drawPentagon(i%2,x,y,walls,(1-fade)*.9*(5+(5-i))/10);


			ofStyle myStyle;
			myStyle.smoothing = true;
			myStyle.color.a = 255*(1-fade);			
			ofSetStyle(myStyle);
			ofPushStyle();
			if(i == currentMenuIndex)
			{	
				float bigWidth = myBigFont.stringWidth(descriptions[i]);
				float bigHeight = myBigFont.stringHeight(descriptions[i]);
				myBigFont.drawString(descriptions[i],centerX - bigWidth/2,centerY+150 + bigHeight/2);
			}
			myStyle.color.g = 120;
			myStyle.color.b = 120;
			ofSetStyle(myStyle);
			float smallWidth = mySmallFont.stringWidth(descriptions[i].substr(0,1));
			float smallHeight = mySmallFont.stringHeight(descriptions[i].substr(0,1));
			mySmallFont.drawString(descriptions[i].substr(0,1),x-smallWidth/2, y+smallHeight/2-4);
			ofPopStyle();
		}
	}
};
