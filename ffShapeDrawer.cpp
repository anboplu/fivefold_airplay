#include "ffShapeDrawer.h"
#include "substitute.h"
#include <vector>
#include <cmath>
#include <iostream>

extern bool bShowAll;
extern bool bShowExit;
extern bool bPathLight;
extern bool bReachLight;
extern bool bWaveLight;
extern bool bHighlight;


void drawPentagon(bool isFlipped, float _x, float _y, vector<bool> drawSides,  float _iter, float radius, int sides)
{
	/*
	if(drawSides.size() != 5)
		drawSides = vector<bool> (5, true);

    double offset = 0;
	if(isFlipped)
		offset = PI;	//should be pi

	//fade is overloaded, -1 is default value and draws pentagon at it's lightest visible setting
	//(0,1) is used for continuous fade amounts
	//0,1,2,3... are used for iterative fading
	CIwColour color;
	color.r = color.g = color.b = 255;
	color.a = 170 - _iter*16;
	if(_iter == -1 || color.a < 32)
		color.a = 32;
	if(_iter > 0 && _iter < 1)
	{
		color.a = 255*_iter - 64;
	}

	//determine pentagon points
	std::vector<ffShapeDrawerPair> pts = std::vector<ffShapeDrawerPair>(sides);
	for(int i = 0; i < sides; i++)
		pts[i] = ffShapeDrawerPair(radius*sin(offset+(PI*2*i/(float)sides)),radius*cos(offset+(PI*2*i/(float)sides)));


	//transform to the position we want
	CIwMat2D oldTrans = Iw2DGetTransformMatrix();
	CIwMat2D revertTrans(oldTrans);
	//make transform matrix
	CIwMat2D newTrans(CIwMat2D::g_Identity);;
	newTrans.SetTrans(CIwVec2(_x,_y));

	Iw2DSetTransformMatrix(newTrans);
	Iw2DSetColour(color);
	CIwSVec2 * pentagonVerts = new CIwSVec2[sides+1];
	for(int i = 0; i <= sides; i++)
		pentagonVerts[i] = CIwSVec2(pts[(i)%(sides)].x, pts[(i)%(sides)].y);
	Iw2DFillPolygon(pentagonVerts,sides+1);
	delete [] pentagonVerts;

	color.a += 48;
	color.g = 170;
	color.r = 170;

	//draw outlines
	for(int i = 0; i < sides; i++)
	{
		if(drawSides[i])
			Iw2DDrawLine(CIwSVec2(pts[i].x,pts[i].y), CIwSVec2(pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y));
	}

	color.a += 32;
	//color.g = 255;
	//color.r = 255;
	for(int i = 0; i < sides; i++)
	{
		Iw2DFillArc(CIwSVec2(pts[(i)%(sides)].x, pts[(i)%(sides)].y),CIwSVec2(2,2),0,360,5);
	}
	Iw2DSetTransformMatrix(revertTrans);
	*/
}

void drawGuy(float _x, float _y, float _iter)
{
	//_iter *= 5;
	float phi = (_iter+100)/10.0;
	float theta = _iter/101.0;	//so it is relatively prime with 100.0
	float tau = _iter/32.0;
	//get a point of the sphere and convert it to xyz
	ofPoint pt;
	pt.z = cos(theta);
	pt.x = pt.y = sin(theta);
	pt.y *= sin(phi);
	pt.x *= cos(phi);
	float bright = 80 * sin(tau);
	CIwColour color;
	pt *= bright;
	color.r = color.b = color.g = 180-bright;
	color.r += pt.x;
	color.g += pt.y;
	color.b += pt.z;
	color.r = 255 - color.r;
	color.g = 255 - color.g;
	color.b = 255 - color.b;

	CIwMat2D oldTrans = Iw2DGetTransformMatrix();
	CIwMat2D revertTrans(oldTrans);
	//make transform matrix
	CIwMat2D trans(CIwMat2D::g_Identity);
	CIwMat2D rot(CIwMat2D::g_Identity);
	trans.SetTrans(CIwVec2(_x,_y));
	rot.SetRot(0x800/4);
	Iw2DSetTransformMatrix(oldTrans.PreMult(trans.PreMult(rot)));
	Iw2DSetColour(color);

	float steps = 15;
	for(int i = 0; i < steps; i++)
	{
		color.a = i*255.0/steps;
		Iw2DSetColour(color);
		Iw2DFillRect(CIwSVec2(-(steps-i)*STUPIDSCALE/2.0,-(steps-i)*STUPIDSCALE/2.0),CIwSVec2((steps-i)*STUPIDSCALE,(steps-i)*STUPIDSCALE));
	}
	Iw2DSetTransformMatrix(revertTrans);
}

template<class T>
struct MaxTracker
{
    bool bInit;
    T val;

    MaxTracker():bInit(false){}
    MaxTracker(T t):bInit(true), val(t){}

    void Boom(T t)
    {
        if(!bInit || t > val)
        {
            bInit = true;
            val = t;
        }
    }
};


void drawLine(CIwSVec2 start, CIwSVec2 end, float thick)
{
	CIwSVec2 v = end-start;
	CIwSVec2 vP(v.y,-v.x);
	vP.NormaliseSafe();
	CIwSVec2 * pts = new CIwSVec2[4];
	pts[0] = start + vP*thick;
	pts[1] = start + v + vP*thick;
	pts[2] = start + v - vP*thick;
	pts[3] = start - vP*thick;
	Iw2DFillPolygon(pts,4);
	delete [] pts;
}

void drawPentagonPulse(bool isFlipped, float _x, float _y,  DrawingParameters dp, int nFrame, vector<bool> drawSides, float radius, bool bExit, int sides)
{
	float pulseSpeed = 1/3.0; //1 iteration per 100 cycles;
	float startingMaxHeight = 255 - 30;
	float heightChangeSlope = 3;
	//float heightChangeSlope = 1./10;
	float a = 30;
    MaxTracker<float> mt(0);
    for(list<ffShapeDrawerPair>::iterator itr = dp.pulsePairs.begin(), etr = dp.pulsePairs.end();
        itr != etr; ++itr)
	{
		float h = startingMaxHeight - (nFrame - itr->x) * heightChangeSlope;
        if(h < startingMaxHeight/8)
            h = startingMaxHeight/8;
		float d = abs(itr->y - pulseSpeed*(nFrame - itr->x))/2.0;
		mt.Boom(h*exp(-(d*d)));
	}

    if(bWaveLight)
        a += mt.val;
    if(bPathLight)
    {
        int nPathLight = 255 / 2 * (1 - float(dp.nPath) / 7);
        if(nPathLight > 0)
            a += nPathLight;
    }
    if(bReachLight && dp.nAge == 0)
        a += 150;
    if(bReachLight && dp.nAge == 1)
        a += 100;

    if(bHighlight)
        a += 200;

    a = a * dp.fFade;

	if(drawSides.size() != 5 || bHighlight)
		drawSides = vector<bool> (5, true);

    double offset = 0;
	if(isFlipped)
		offset = PI;

	//determine pentagon points
	std::vector<ffShapeDrawerPair> pts = std::vector<ffShapeDrawerPair>(sides);
	for(int i = 0; i < sides; i++)
		pts[i] = ffShapeDrawerPair(radius*sin(offset+(PI*2*i/(float)sides)),radius*cos(offset+(PI*2*i/(float)sides)));

	//transform to the position we want
	CIwMat2D oldTrans = Iw2DGetTransformMatrix();
	CIwMat2D revertTrans(oldTrans);

	//make transform matrix
	CIwMat2D newTrans(CIwMat2D::g_Identity);
	newTrans.SetTrans(CIwVec2(_x,_y));
	Iw2DSetTransformMatrix(oldTrans.PreMult(newTrans));

	CIwColour color;
	color.a = quClamp<float>(a,0,255);
	color.r = color.g =color.b = 210;
	Iw2DSetColour(color);

	CIwSVec2 * pentagonVerts = new CIwSVec2[sides+1];
	for(int i = 0; i <= sides; i++)
		pentagonVerts[i] = CIwSVec2(pts[(i)%(sides)].x, pts[(i)%(sides)].y);
	Iw2DFillPolygon(pentagonVerts,sides+1);
	delete [] pentagonVerts;

	color.r = 240;
	color.g = 200;
	color.b = 120;
	Iw2DSetColour(color);

	CIwColour c2;
	c2.r = c2.g = c2.b = c2.a = 127;

	for(int i = 0; i < sides; i++)
	{
		if(drawSides[i])
		{
			Iw2DSetColour(color);
			drawLine(CIwSVec2(pts[i].x,pts[i].y), CIwSVec2(pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y),2*STUPIDSCALE);
		}
		else
		{
			Iw2DSetColour(c2);
			drawLine(CIwSVec2(pts[i].x,pts[i].y), CIwSVec2(pts[(i+1)%(sides)].x, pts[(i+1)%(sides)].y),0.3*STUPIDSCALE);
		}
	}

	color.r = color.g = color.b = 255;
	for(int i = 0; i < sides; i++)
	{
		Iw2DFillArc(CIwSVec2(pts[(i)%(sides)].x, pts[(i)%(sides)].y),CIwSVec2(2,2)*STUPIDSCALE,0,IW_ANGLE_2PI,10);
	}

	if(bExit)
	{
		color.a = 255; // should not need this but just in case
		color.r = color.g = color.b = 255;
		Iw2DSetColour(color);

		//draw the hole
		Iw2DFillRect(CIwSVec2(-radius/4,-radius/2),CIwSVec2(radius/2,radius));


		color.r = color.g = color.b = 140;
		
		CIwSVec2 * doorVerts = new CIwSVec2[4];
		doorVerts[0] = CIwSVec2(-radius/4.,radius/2.);
		doorVerts[1] = CIwSVec2(-radius/4.+radius/2.-radius/20.,radius/2.+radius/10.);
		doorVerts[2] = CIwSVec2(-radius/4.+radius/2.-radius/20.,radius/2.-radius+radius/10.);
		doorVerts[3] = CIwSVec2(-radius/4.,radius/2.-radius);
		Iw2DFillPolygon(doorVerts,4);
		delete doorVerts;
	}    
    
   Iw2DSetTransformMatrix(revertTrans);
}