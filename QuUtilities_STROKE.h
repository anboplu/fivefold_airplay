#pragma once

template <typename T>
inline T quAbs(const T & aVal)
{
	if( aVal < 0)
		return -aVal;
	return aVal;
}

template <typename T>
inline T quClamp(const T aVal, T min, T max)
{
	if( aVal < min)
		return min;
	else if (aVal > max)
		return max;
	else
		return aVal;
}

inline float quRandf()
{
	return ((float)rand())/RAND_MAX;
}
inline float quRandfSym()
{
	return -1+2*((float)rand())/RAND_MAX;
}
//returns random int from min inclusive to max exclusive
inline int quRandRange(int min, int max)
{
	return min + rand()%(max-min);
}



struct QuTimer
{
	
	QuTimer(int aStart = 0, int aTarget = 0):mStart(aStart),mCurrent(aStart),mTarget(aTarget)
	{}
	bool isSet()
	{
		return mTarget != mStart;
	}
	void update()
	{
		mCurrent++;
	}
	QuTimer & operator++(int unused)
	{
		update();
		return *this;
	}
	QuTimer & operator--(int unused)
	{
		if(mCurrent > 0)
			mCurrent--;
		return *this;
	}
	void setTarget(int aTarget)
	{
		mTarget = aTarget;
	}
	void reset()
	{
		mCurrent = mStart;
	}
	void setTargetAndReset(int aTarget)
	{
		setTarget(aTarget);
		reset();
	}
	void expire()
	{
		mCurrent = mTarget;
	}
	//timer expires when mCurrent is >= mTarget
	bool isExpired()
	{
		return mCurrent >= mTarget;
	}
	int getTimeSinceStart()
	{
		return mCurrent-mStart;
	}
	float getLinear()
	{
		if(isExpired())
			return 1;
		return (float)(mCurrent-mStart)/(float)(mTarget-mStart);
	}
	float getLinear(float l, float r)
	{
		return getLinear()*(r-l)+l;
	}
	float getSquareRoot()
	{
		return sqrt(getLinear());
	}
	float getSquare()
	{
		float r = getLinear();
		return r*r;
	}
	float getCubed(float l = 0, float r = 1)
	{
		float v = getLinear();
		return v*v*v*(r-l)+l;
	}
	//0 and 0 and 1, max at 0.5
	float getUpsidedownParabola()
	{
		float x = getLinear();
		return (-(x-0.5)*(x-0.5) + 0.25)*4;
	}
	//1 at 0 and 1, 0 at 0.5
	float getParabola(float l = 0, float r = 1)
	{
		return (1-getUpsidedownParabola())*(r-l)+l;
	}
private:
	int mStart,mCurrent,mTarget;
};

template <typename T>
struct QuPoint
{
	T x,y;
	QuPoint(T ax, T ay)
	{
		x = ax;
		y = ay;
	}
};

struct QuMouseDragged
{
	int dx, dy;
	int cx, cy;
	bool isMouseDown;
	float mThreshSquared;
	void setThresh(float aThresh)
	{
		mThreshSquared = aThresh*aThresh;
	}
	QuMouseDragged()
	{
		dx = dy = cx = cy = 0;
		setThresh(20);
		isMouseDown = false;
	}
	float getChangeNorm()
	{
		QuPoint<float> c = getChange();
		return sqrt(c.x*c.x + c.y*c.y);
	}
	QuPoint<float> getChange()
	{
		if(isMouseDown)
			return QuPoint<float>((dx-cx),(dy-cy));
		return QuPoint<float>(0,0);
	}
	bool mouseReleased(int x, int y)
	{
		isMouseDown = false;
		int deltax = (dx-x);
		int deltay = (dy-y);
		//test if threshold is exceeded
		if(deltax*deltax + deltay*deltay >= mThreshSquared)
			return true;
		return false;
	}
	void mousePressed(int x, int y)
	{
		cx = dx = x;
		cy = dy = y;
		isMouseDown = true;
	}
	void mouseDragged(int x, int y)
	{
		cx = x; cy = y;
	}
};