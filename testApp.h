#pragma once
#include "PenMaze.h"

class testApp
{
    private:
		//PentagonLinearMenu* pMenu;
        Game* pGm;
        PentaMaze* pPmz;

        bool bRight;
        bool bLeft;
        bool bUp;
        bool bDown;

        float fZoom;

	public:
		//does not get called until context is destroyed, careful with this guy
        ~testApp();

		void setup();
		void update();
		void draw();
		void terminate(){}

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

};

