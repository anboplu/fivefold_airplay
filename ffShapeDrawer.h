#ifndef FF_SHAPE_DRAWER
#define FF_SHAPE_DRAWER

#include <vector>
#include <list>
#include "Iw2D.h"
using namespace std;

struct ffShapeDrawerPair
{
	float x,y;
	ffShapeDrawerPair()
	{
		x = y = 0;
	}
	ffShapeDrawerPair(float _x, float _y)
	{
		x = _x;
		y = _y;
	}
};

struct DrawingParameters
{
    list<ffShapeDrawerPair> & pulsePairs;
    int nPath;
    int nAge;
    float fFade;

    DrawingParameters(list<ffShapeDrawerPair> & pulsePairs_, int nPath_, int nAge_, float fFade_)
        :pulsePairs(pulsePairs_), nPath(nPath_), nAge(nAge_), fFade(fFade_){}
};

void drawPentagon(bool isFlipped, float _x, float _y, vector<bool> drawSides, float _iter = -1, float radius = 50, int sides = 5);
void drawPentagonPulse(bool isFlipped, float _x, float _y,  DrawingParameters dp, int nFrame, vector<bool> drawSides, float radius, bool bExit, int sides = 5);
void drawGuy(float _x, float _y, float _iter);

#endif