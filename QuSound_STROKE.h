#pragma once
#include <string>
#include <map>
#include "QuUtilities_STROKE.h"
#include "s3eSound.h"
#include "s3eFile.h"
#include "IwSound.h"

using namespace std;
class QuSoundStruct
{
	QuTimer mVolumeTimer;
	float mMaxVolume;
	float mMinVolume;
	bool mLoop;
	bool mMultiPlay;
	int mChannel;
	CIwSoundSpec * mSound;
	int32 mFileSize;
	void loadSound(string filename)
	{
		//NOTE this requires sound to have been loaded by resource manager earlier
		mSound = (CIwSoundSpec*)IwGetResManager()->GetResNamed(filename.c_str(), IW_SOUND_RESTYPE_SPEC);
	}
public:
	static int32 OnEndOfSample(void * _systemData, void* userData)
	{
		return 0;
	}
	QuSoundStruct(string filename, bool stream, bool aLoop, bool multiplay)
	{
		loadSound(filename);

		//currently only multiplay supported
		mMultiPlay = true;
		mLoop = aLoop;
		mMaxVolume = 1;
		mMinVolume = 0;
		mChannel = -1;
	}
	~QuSoundStruct()
	{
	}
	void play()
	{
		//try and get a free mChannel
		//TODO should use s3eSoundChannelRegister with S3E_CHANNEL_END_SAMPLE so we know when the sound has finished playing
		mSound->Play();
	}
	void stop()
	{
		IwGetSoundManager()->StopSoundSpec(mSound);
	}
	float getVolume()
	{
		float t = mVolumeTimer.getLinear();
		return (mMinVolume*(1-t) + mMaxVolume*t);
	}
	void update()
	{
		float t = mVolumeTimer.getLinear();
		//TODO set volume to getVolume()*S3E_SOUND_MAX_VOLUME
		mVolumeTimer++;
	}
	void setFade(int time, float max)
	{
		if(mVolumeTimer.isSet())
			mMinVolume = getVolume();
		else mMinVolume = 0;
		mMaxVolume = max;
		mVolumeTimer.setTargetAndReset(time);
	}
	bool isFadingOut()
	{
		return mMaxVolume <= mMinVolume;
	}
	void setLoop(bool looping)
	{
		mLoop = looping;
	}
	void setMultiplay(bool multiplay)
	{
		//TODO
	}
};

class QuSoundManager
{
	static QuSoundManager * mSelf;
	map<string,QuSoundStruct *> mSounds;
public:
	int frameCounter;
public:
	static QuSoundManager & getRef()
	{
		//NOTE not thread safe...
		if(mSelf == NULL)
			mSelf = new QuSoundManager();
		return *mSelf;
	}
	static void destroyQuSoundManager()
	{
		delete mSelf;
		mSelf = NULL;
	}
	~QuSoundManager()
	{
		for(map<string,QuSoundStruct *>::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			delete i->second;
	}
	void loadSound(string filename,bool stream = false, bool loop = false, bool multiplay = false)
	{
		if (mSounds.find(filename) != mSounds.end())
			return;
		mSounds[filename] = new QuSoundStruct(filename,stream,loop,multiplay);
	}
	void playSound(string filename,bool looping = false)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			mSounds[filename]->play();
			mSounds[filename]->setLoop(looping);
		}
	}
	void stopSound(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			mSounds[filename]->stop();
	}

	void update()
	{
		IwGetSoundManager()->Update();
		for(map<string,QuSoundStruct *>::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second->update();
	}
	void setFade(string filename, bool isFadeIn)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			if(isFadeIn)
				mSounds[filename]->setFade(20,1);
			else
				mSounds[filename]->setFade(20,0);
		}
	}
	bool isFadingOut(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename]->isFadingOut();
	}
private:
	QuSoundManager()
	{
		frameCounter = 0;
	}
};