#include "iw2D.h"
#include "s3e.h"
#include "IwSound.h"
#include "QuSound_STROKE.h"
#include "testApp.h"

QuSoundManager * QuSoundManager::mSelf = NULL;

static const int MSPF = 30;
testApp * stroke;

void keyboardcb(void* systemData, void* userData)
{
	s3eKeyboardEvent* event = (s3eKeyboardEvent*)systemData;
	if(event->m_Pressed)
		stroke->keyPressed(event->m_Key);
	else
		stroke->keyReleased(event->m_Key);
}
void touchDown(s3ePointerEvent* event)
{
	if(event->m_Pressed)
		stroke->mousePressed(event->m_x,event->m_y,event->m_Button);
	else
		stroke->mouseReleased(event->m_x,event->m_y,event->m_Button);
		
}

void touchMotion(s3ePointerMotionEvent* event)
{
}

bool isQuit()
{
    bool rtn = s3eDeviceCheckQuitRequest()
    || (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
    || (s3eKeyboardGetState(s3eKeyAbsBSK) & S3E_KEY_STATE_PRESSED);
    if (rtn)
        s3eDebugTracePrintf("quiting example");
    return rtn;
}

void yieldCycle()
{
	static int time = 0;
	time = s3eTimerGetMs()-time;
	if(time > MSPF)
		s3eDeviceYield(0);
	else
		s3eDeviceYield(MSPF-time);
	time = s3eTimerGetMs();
}
void mainInit()
{
	//initialize
	Iw2DInit();
	s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, (s3eCallback)keyboardcb, NULL);
	s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)touchDown, NULL);
    s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)touchMotion, NULL);
	IwSoundInit();
	IwResManagerInit();
	IwGetResManager()->AddHandler(new CIwResHandlerWAV);
}

void mainTerm()
{
	QuSoundManager::destroyQuSoundManager(); 
	IwResManagerTerminate();
	IwSoundTerminate();
	s3eKeyboardUnRegister(S3E_KEYBOARD_KEY_EVENT, (s3eCallback)keyboardcb);
	s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)touchDown);
    s3ePointerUnRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)touchMotion);
	Iw2DTerminate();
}

int main()
{
	mainInit();

	stroke = new testApp();
	stroke->setup();
	while (!isQuit())
	{
		s3eKeyboardUpdate();
		s3ePointerUpdate();

		CIwColour color;
		color.Set(127,127,127);
		Iw2DSurfaceClear(color);

		stroke->update();
		stroke->draw();

		Iw2DSurfaceShow();

		QuSoundManager::getRef().frameCounter++;
		yieldCycle();
	}
	stroke->terminate();
	delete stroke;

	mainTerm();
}

