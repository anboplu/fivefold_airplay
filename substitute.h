#pragma once
#include <string>
#include <vector>
#include <list>
#include <stdlib.h>
#include <cmath>
#include <fstream>
#include "s3eSurface.h"
#include "QuSound_STROKE.h"

#define SCREEN_WIDTH int(s3eSurfaceGetInt(S3E_SURFACE_WIDTH))
#define SCREEN_HEIGHT int(s3eSurfaceGetInt(S3E_SURFACE_HEIGHT))
#define CURRENT_FRAME QuSoundManager::getRef().frameCounter

#define PI 3.14159265
#define HALF_PI 3.14159265/2
#define TWO_PI 3.14159265*2

#define STUPIDSCALE 50.0f



struct ofPoint
{
	float x, y, z;
	ofPoint& operator *= (float c){x *= c; y *= c; z*=c; return *this;}
};

namespace Gui
{
    typedef int Crd;
    typedef unsigned char Byte;

    struct Color
    {
        Byte B,G,R;
        Byte nTransparent;  // 0 - fully transparent

        Color():R(0), G(0), B(0), nTransparent(Byte(255)){}
        Color(Byte R_, Byte G_, Byte B_, Byte nTransparent_ = Byte(255)):R(R_), G(G_), B(B_), nTransparent(nTransparent_){}
        Color(int R_, int G_, int B_, int nTransparent_ = 255)  // for convenience
            :R(Byte(R_)), G(Byte(G_)), B(Byte(B_)), nTransparent(Byte(nTransparent_)){}
    };

    // two fully transparent colors are equal, otherwise compare color components only
    inline bool operator == (const Color& c1, const Color& c2) 
    {
        if (c1.nTransparent == 0 && c2.nTransparent == 0)
            return true;
        if (c1.nTransparent != 0 && c2.nTransparent == 0)
            return false;
        if (c1.nTransparent == 0 && c2.nTransparent != 0)
            return false;
        return (c1.B == c2.B) && (c1.G == c2.G) && (c1.R == c2.R);
    }

    std::string ColorToString(Color c); // for output

    struct Point
    {
        Crd x, y;

        Point():x(0), y(0){}
        Point(Crd x_, Crd y_):x(x_), y(y_){}

        Point& operator += (const Point& p){x += p.x; y += p.y; return *this;}
        Point& operator -= (const Point& p){x -= p.x; y -= p.y; return *this;}
        Point& operator *= (Crd c){x *= c; y *= c; return *this;}
    };

    inline Point operator - (const Point& p) {return Point(-p.x, -p.y);}
    inline Point operator + (const Point& p1, const Point& p2) {return Point(p1) += p2;}
    inline Point operator - (const Point& p1, const Point& p2) {return Point(p1) -= p2;}
    inline bool operator == (const Point& p1, const Point& p2) {return p1.x == p2.x && p1.y == p2.y;}
    inline bool operator != (const Point& p1, const Point& p2) {return p1.x != p2.x || p1.y != p2.y;}

    inline Point& operator * (const Point& p, Crd c) {return Point(p) *= c;}

    inline std::ostream& operator << (std::ostream& ofs, Point f){return ofs << f.x << " " << f.y;}
    inline std::istream& operator >> (std::istream& ifs, Point& f){return ifs >> f.x >> f.y;}

    struct fPoint
    {
        float x,y;

        fPoint():x(0), y(0){}
        fPoint(double x_, double y_):x(float(x_)), y(float(y_)){}

        fPoint(Point p): x(float(p.x)), y(float(p.y)){}

        Point ToPnt() const {return Point(int(x), int(y));}

        void Normalize(float f = 1)
        {
            if(x == 0 && y == 0)
                return;
            
            float d = sqrt(x * x + y * y);
            x /= d;
            y /= d;
            x *= f;
            y *= f;
        }
        float Length() const {return sqrt(x*x + y*y);}
    };

    inline fPoint& operator += (fPoint& f1, const fPoint& f2){f1.x += f2.x; f1.y += f2.y; return f1;}
    inline fPoint& operator -= (fPoint& f1, const fPoint& f2){f1.x -= f2.x; f1.y -= f2.y; return f1;}
    inline fPoint operator + (const fPoint& f1, const fPoint& f2){fPoint f(f1); return f += f2;}
    inline fPoint operator - (const fPoint& f1, const fPoint& f2){fPoint f(f1); return f -= f2;}
    inline bool operator == (const fPoint& f1, const fPoint& f2){return (f1.x == f2.x) && (f1.y == f2.y);}
    inline bool operator != (const fPoint& f1, const fPoint& f2){return (f1.x != f2.x) || (f1.y != f2.y);}
    inline fPoint& operator /= (fPoint& f1, float f){f1.x /= f; f1.y /= f; return f1;}
    inline fPoint& operator *= (fPoint& f1, float f){f1.x *= f; f1.y *= f; return f1;}
    inline fPoint operator / (const fPoint& f1, float f){fPoint ff(f1); return ff /= f;}
    inline fPoint operator * (const fPoint& f1, float f){fPoint ff(f1); return ff *= f;}

    inline std::ostream& operator << (std::ostream& ofs, fPoint f){ofs << f.x << " " << f.y; return ofs;}
    inline std::istream& operator >> (std::istream& ifs, fPoint& f){ifs >> f.x >> f.y; return ifs;}

    struct Size
    {
        Crd x, y;

        Size():x(0), y(0){}
        Size(Crd x_, Crd y_):x(x_), y(y_){}

        Crd Area() const {return x * y;}
    };

    inline bool operator == (const Size& p1, const Size& p2) {return p1.x == p2.x && p1.y == p2.y;}
    inline bool operator != (const Size& p1, const Size& p2) {return p1.x != p2.x || p1.y != p2.y;}

    inline std::ostream& operator << (std::ostream& ofs, Size s){return ofs << s.x << " " << s.y;}
    inline std::istream& operator >> (std::istream& ifs, Size& s){return ifs >> s.x >> s.y;}

    struct Rectangle    // rectangle, cannot have negative size, zero rectangle has size (0,0)
    {
        Point p;
        Size sz;

        Rectangle():p(0,0), sz(0,0){}
        Rectangle(Size sz_):p(Point(0,0)), sz(sz_){Normalize();}
        Rectangle(Point p_, Size sz_):p(p_), sz(sz_){Normalize();}
        Rectangle(Point p1, Point p2):p(p1), sz(Size(p2.x - p1.x, p2.y - p1.y)){Normalize();}
        Rectangle(Crd l, Crd t, Crd r, Crd b):p(l, t), sz(r - l, b - t){Normalize();}

        void Normalize()
        {
            if(sz.x < 0 || sz.y < 0)
                sz = Size(0,0);
        }
        
        Crd Left() const {return p.x;}
        Crd Right() const{return p.x + sz.x;}
        Crd Top() const{return p.y;}
        Crd Bottom() const{return p.y + sz.y;}

        Point GetBottomRight() const{return Point(p.x + sz.x, p.y + sz.y);}

        Crd Area() const{return sz.Area();}
    };

    inline std::ostream& operator << (std::ostream& ofs, Rectangle r)
        {return ofs << r.p.x << " " << r.p.y << " " << r.sz.x << " " << r.sz.y;}
    inline std::istream& operator >> (std::istream& ifs, Rectangle& r)
        {return ifs >> r.p.x >> r.p.y >> r.sz.x >> r.sz.y;}

    bool InsideRectangle(Rectangle r, Point p);   // is the point inside the rectangle?

    Point Center(Rectangle r);
    Point RandomPnt(Rectangle r);

    Rectangle operator + (const Rectangle& r, const Point& p);    // displace
    Rectangle operator + (const Rectangle& r1, const Rectangle& r2);

    Rectangle Intersect(const Rectangle& r1, const Rectangle& r2);    // return intersection of two rectangles
    std::string RectangleToString(Rectangle r); // for output

    struct MatrixErrorInfo  // stores info for out of range exception
    {
        Size sz;
        Point p;

        MatrixErrorInfo(Size sz_, Point p_)
            :sz(sz_), p(p_){}

        std::string GetErrorMessage() const;
    };
}